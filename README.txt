This module is a workaround for a well known bug in Drupal core:

https://www.drupal.org/node/1694574

This module is addressed to developers, since you need to implement 'hook_form_alter' (or 'hook_form_FORM_ID_alter') in order for it to work.

How it works:

1 - A tiny JS is injected in the page containing the form which fails on anonymous AJAX requests
2 - The script performs an AJAX POST request to check whether the given form_build_id exists in cache
3 - The module then looks for the form in the cache_form table. If it doesn't exist, it recreates the record and sends back and AJAX command to update the form_build_id


How to use it:

1 - Implement hook_form_alter on the form where you want to have the AJAX issue fixed for anonymous users
2 - Initialize the module by calling the method 'rebuild_ajax_forms_initialize' within 'hook_form_FORM_ID_alter'
3 - Provide a callback function and needed arguments to rebuild the form in case it's not found in cache (if no callback function is provided, the module will try to load the form with 'drupal_get_form', although this is not always reliable)
4 - Register your callback by implementing hook_rebuild_ajax_forms_callback_alter in your module

Example

Imagine you have a site which uses Drupal Commerce to sell products.
You have installed and enabled Commerce Ajax Add to Cart.

In order for anonymous users to be able to ALWAYS add products to cart, you can use this module.

This is the code needed:

<?php
/**
* Implements hook_form_FORM_ID_alter().
*/
function mymodule_form_views_form_commerce_cart_form_default_alter(&$form, &$form_state) {
  // Provide a callback function which will rebuild the form if needed
  $callback = 'mymodule_rebuild_form_callback';
  // Provide any arguments needed to rebuild the form
  $args = array();
  if (isset($form_state['context']['entity_id'])) {
    // We will need the nid of the product to rebuild the add to cart form
    $nid = $form_state['context']['entity_id'];
  }
  // Initialize Rebuild AJAX Forms
  rebuild_ajax_forms_initialize($form, $callback, $args);
}

function mymodule_rebuild_form_callback($form_id, $args) {
  $form = array();
  if (isset($args['nid'])) {
    // Retrieve node
    $node = node_load((int) $args['nid']);
    // Let Drupal Commerce to the work
    // Force creation of the form by loading the field which displays the add to cart form
    $output = field_view_field('node', $node, 'field_product_reference');
    if (isset($output[0])) {
      $form = $output[0];
    }
  }
  return $form;
}

/**
 *  Implements hook_rebuild_ajax_forms_callback_alter()
 *  
 */
function mymodule_rebuild_ajax_forms_callback_alter(&$registered_callbacks) {
  $registered_callbacks[] = 'mymodule_rebuild_form_callback';
}
