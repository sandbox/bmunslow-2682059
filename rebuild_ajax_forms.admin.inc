<?php
/**
 * @file
 * Code for the module settings.
 */

/**
 * Form constructor for the Global Config form.
 */
function rebuild_ajax_forms_form($form, &$form_state) {
  $form['rebuild_ajax_forms_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable debugging'),
    '#description' => t('Log debug messages.'),
    '#default_value' => variable_get('rebuild_ajax_forms_debug', 0),
  );

  return system_settings_form($form);
}
