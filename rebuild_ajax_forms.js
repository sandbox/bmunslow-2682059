/**
 * @file
 * Ajax add to cart module.
 */

(function ($, Drupal, window, document, undefined) {
  Drupal.behaviors.rebuild_ajax_forms = {
    attach: function (context, settings) {
      // Add an extra function to the Drupal ajax object
      // which allows us to trigger an ajax response without
      // an element that triggers it.
      // @see https://www.deeson.co.uk/labs/trigger-drupal-managed-ajax-calls-any-time-drupal-7
      if ($.type(Drupal.ajax.prototype.specifiedResponse) == 'undefined') {
        Drupal.ajax.prototype.specifiedResponse = function() {
          var ajax = this;
          // Do not perform another ajax command if one is already in progress.
          if (ajax.ajaxing) {
            return false;
          }
          try {
            $.ajax(ajax.options);
          }
          catch (err) {
            alert("An error occurred in: " + ajax.options.url);
            return false;
          }
          return false;
        };
      }
      
      // Process each requested form in the page, request form_build_id
      for (id in settings.rebuild_ajax_forms.instances) {
        // Define a custom ajax action not associated with an element.
        var custom_settings = {};
        custom_settings.url = '/rebuild_ajax_forms';
        custom_settings.event = 'onload';
        custom_settings.keypress = false;
        custom_settings.prevent = false;
        custom_settings.progress = {type: 'none'};
        custom_settings.submit = {form_data: settings.rebuild_ajax_forms.instances[id]};
        Drupal.ajax['custom_ajax_action'] = new Drupal.ajax(null, $(document.body), custom_settings);
        $('form[id|="' + id + '"]').once('rebuild_ajax_forms', function(){
          Drupal.ajax['custom_ajax_action'].specifiedResponse();
        });
      }
    }
  };
})(jQuery, Drupal, this, this.document);
